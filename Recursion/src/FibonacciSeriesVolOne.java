public class FibonacciSeriesVolOne {
    public static long fibonnaci(long n){
        if(n == 1 || n == 2){
            return 1;
        }else{
            return fibonnaci(n -1) + fibonnaci(n -2);
        }
    }

    public static void main(String[] args) {
        System.out.println(fibonnaci(4));
        System.out.println(fibonnaci(6));
        System.out.println(fibonnaci(10));
        System.out.println(fibonnaci(12));
        System.out.println(fibonnaci(18));
        System.out.println(fibonnaci(29));
    }
}
