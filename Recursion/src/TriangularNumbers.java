public class TriangularNumbers {
    public static long triangle(long n) {
        if (n == 1) {
            return 1;
        } else {
            return n + triangle(n - 1);
        }
    }

    public static void main(String[] args) {
        System.out.println(triangle(50));
    }
}
